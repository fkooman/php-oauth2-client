<?php

declare(strict_types=1);

/*
 * Copyright (c) 2016-2025 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

require_once \dirname(__DIR__) . '/vendor/autoload.php';

use fkooman\OAuth\Client\Http\CurlHttpClient;
use fkooman\OAuth\Client\NullLogger;
use fkooman\OAuth\Client\Provider;

/*
 * Perform OAuth / OpenID Connect Discovery.
 *
 * Example URLs:
 * - https://idp.tuxed.net/.well-known/openid-configuration
 * - https://connect.surfconext.nl/.well-known/openid-configuration
 * - https://accounts.google.com/.well-known/openid-configuration
 * - https://login.microsoftonline.com/common/v2.0/.well-known/openid-configuration
 */
try {
    $discoveryUrl = null;
    $allowInsecureHttp = false;
    for ($i = 1; $i < $argc; $i++) {
        if ('--insecure' === $argv[$i]) {
            $allowInsecureHttp = true;

            continue;
        }
        $discoveryUrl = $argv[$i];
    }

    if (null === $discoveryUrl) {
        throw new Exception('provide OpenID Connect or OAuth Discovery URL');
    }

    $httpClient = new CurlHttpClient(new NullLogger());
    if ($allowInsecureHttp) {
        $httpClient = $httpClient->withAllowInsecureHttp(true);
    }

    $p = Provider::fromDiscovery(
        $httpClient,
        $discoveryUrl,
        'demo_client',
        'demo_secret'
    );

    echo $p->save();
} catch (Exception $e) {
    echo 'ERROR: ' . $e->getMessage() . \PHP_EOL;
    exit(1);
}
