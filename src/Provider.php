<?php

declare(strict_types=1);

/*
 * Copyright (c) 2016-2025 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\OAuth\Client;

use fkooman\OAuth\Client\Http\HttpClientInterface;
use fkooman\OAuth\Client\Http\Request;

class Provider
{
    private string $clientId;
    private ?string $clientSecret;
    private string $authorizationEndpoint;
    private string $tokenEndpoint;
    private string $issuerIdentity;

    public function __construct(string $clientId, ?string $clientSecret, string $authorizationEndpoint, string $tokenEndpoint, string $issuerIdentity)
    {
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
        $this->authorizationEndpoint = $authorizationEndpoint;
        $this->tokenEndpoint = $tokenEndpoint;
        $this->issuerIdentity = $issuerIdentity;
    }

    /**
     * @see RFC 8414 "OAuth 2.0 Authorization Server Metadata"
     * @see https://www.rfc-editor.org/rfc/rfc8414
     */
    public static function fromDiscovery(HttpClientInterface $httpClient, string $discoveryUrl, string $clientId, ?string $clientSecret): self
    {
        $discoveryData = $httpClient->send(Request::get($discoveryUrl))->json();

        return self::load(
            Json::encode(
                array_merge(
                    $discoveryData,
                    [
                        'client_id' => $clientId,
                        'client_secret' => $clientSecret,
                    ]
                )
            )
        );
    }

    public function save(): string
    {
        return Json::encode(
            [
                'client_id' => $this->clientId,
                'client_secret' => $this->clientSecret,
                'authorization_endpoint' => $this->authorizationEndpoint,
                'token_endpoint' => $this->tokenEndpoint,
                'issuer' => $this->issuerIdentity,
            ]
        );
    }

    public static function load(string $jsonString): self
    {
        $providerData = Json::decode($jsonString);

        return new self(
            Extractor::requireString($providerData, 'client_id'),
            Extractor::optionalString($providerData, 'client_secret'),
            Extractor::requireString($providerData, 'authorization_endpoint'),
            Extractor::requireString($providerData, 'token_endpoint'),
            Extractor::requireString($providerData, 'issuer')
        );
    }

    public function providerId(): string
    {
        return \sprintf('%s|%s', $this->authorizationEndpoint(), $this->clientId());
    }

    /**
     * @see https://tools.ietf.org/html/rfc6749#section-2.2
     */
    public function clientId(): string
    {
        return $this->clientId;
    }

    /**
     * @see https://tools.ietf.org/html/rfc6749#section-2.3.1
     */
    public function clientSecret(): ?string
    {
        return $this->clientSecret;
    }

    /**
     * @see https://tools.ietf.org/html/rfc6749#section-3.1
     */
    public function authorizationEndpoint(): string
    {
        return $this->authorizationEndpoint;
    }

    /**
     * @see https://tools.ietf.org/html/rfc6749#section-3.2
     */
    public function tokenEndpoint(): string
    {
        return $this->tokenEndpoint;
    }

    /**
     * @see https://www.rfc-editor.org/rfc/rfc9207.html
     */
    public function issuerIdentity(): string
    {
        return $this->issuerIdentity;
    }
}
