<?php

declare(strict_types=1);

/*
 * Copyright (c) 2016-2025 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\OAuth\Client;

use DateTimeImmutable;
use fkooman\OAuth\Client\Exception\AuthorizeException;
use fkooman\OAuth\Client\Exception\OAuthException;
use fkooman\OAuth\Client\Exception\TokenException;
use fkooman\OAuth\Client\Http\HttpClientInterface;
use fkooman\OAuth\Client\Http\Request;
use fkooman\OAuth\Client\Http\Response;

class OAuthClient
{
    protected DateTimeImmutable $dateTime;
    private SessionInterface $session;
    private TokenStorageInterface $tokenStorage;
    private HttpClientInterface $httpClient;

    public function __construct(SessionInterface $session, TokenStorageInterface $tokenStorage, HttpClientInterface $httpClient)
    {
        $this->tokenStorage = $tokenStorage;
        $this->httpClient = $httpClient;
        $this->session = $session;
        $this->dateTime = new DateTimeImmutable();
    }

    /**
     * Perform a GET request, convenience wrapper for ::send().
     */
    public function get(Provider $provider, string $userId, Scope $requestScope, string $requestUri, array $requestHeaders = []): ?Response
    {
        return $this->send($provider, $userId, $requestScope, Request::get($requestUri, $requestHeaders));
    }

    /**
     * Perform a POST request, convenience wrapper for ::send().
     */
    public function post(Provider $provider, string $userId, Scope $requestScope, string $requestUri, array $postBody, array $requestHeaders = []): ?Response
    {
        return $this->send($provider, $userId, $requestScope, Request::post($requestUri, $postBody, $requestHeaders));
    }

    /**
     * Perform a HTTP request.
     */
    public function send(Provider $provider, string $userId, Scope $requestScope, Request $request): ?Response
    {
        if (null === $accessToken = $this->getAccessToken($provider, $userId, $requestScope)) {
            return null;
        }

        if ($accessToken->isExpired($this->dateTime)) {
            // access_token is expired, try to refresh it
            if (null === $accessToken = $this->refreshAccessToken($provider, $userId, $accessToken)) {
                // didn't work
                return null;
            }
        }

        // add Authorization header to the request headers
        $request->setHeader('Authorization', \sprintf('Bearer %s', $accessToken->accessToken()));

        $response = $this->httpClient->send($request);
        if (401 === $response->statusCode()) {
            // the access_token was not accepted, but isn't expired, we assume
            // the user revoked it, also no need to try with refresh_token
            $this->tokenStorage->deleteAccessToken($userId, $accessToken);

            return null;
        }

        return $response;
    }

    /**
     * Obtain an authorization request URL to start the authorization process
     * at the OAuth provider.
     *
     * @param Scope $scope       the space separated scope tokens
     * @param string $redirectUri the URL registered at the OAuth provider, to
     *                            be redirected back to
     *
     * @return string the authorization request URL
     *
     * @see https://tools.ietf.org/html/rfc6749#section-3.3
     * @see https://tools.ietf.org/html/rfc6749#section-3.1.2
     */
    public function getAuthorizeUri(Provider $provider, string $userId, Scope $scope, string $redirectUri): string
    {
        $codeVerifier = Base64UrlSafe::encodeUnpadded($this->randomBytes());
        $queryParameters = [
            'client_id' => $provider->clientId(),
            'redirect_uri' => $redirectUri,
            'scope' => (string) $scope,
            'state' => Base64UrlSafe::encodeUnpadded($this->randomBytes()),
            'response_type' => 'code',
            'code_challenge_method' => 'S256',
            'code_challenge' => Base64UrlSafe::encodeUnpadded(hash('sha256', $codeVerifier, true)),
        ];

        $authorizeUri = \sprintf(
            '%s%s%s',
            $provider->authorizationEndpoint(),
            false === strpos($provider->authorizationEndpoint(), '?') ? '?' : '&',
            http_build_query($queryParameters)
        );
        $this->session->set(
            '_oauth2_session',
            Json::encode(
                array_merge(
                    $queryParameters,
                    [
                        'user_id' => $userId,
                        'provider_id' => $provider->providerId(),
                        'code_verifier' => $codeVerifier,
                    ]
                )
            )
        );

        return $authorizeUri;
    }

    public function handleCallback(Provider $provider, string $userId, array $getData): void
    {
        if (\array_key_exists('error', $getData)) {
            // remove the session
            $this->session->take('_oauth2_session');

            throw new AuthorizeException($getData['error'], \array_key_exists('error_description', $getData) ? $getData['error_description'] : null);
        }

        if (false === \array_key_exists('code', $getData)) {
            throw new OAuthException('missing "code" query parameter from server response');
        }

        if (false === \array_key_exists('state', $getData)) {
            throw new OAuthException('missing "state" query parameter from server response');
        }

        $this->doHandleCallback($provider, $userId, $getData['code'], $getData['state']);
    }

    protected function randomBytes(): string
    {
        return random_bytes(32);
    }

    /**
     * @param string $responseCode  the code passed to the "code" query parameter on the callback URL
     * @param string $responseState the state passed to the "state" query parameter on the callback URL
     */
    private function doHandleCallback(Provider $provider, string $userId, string $responseCode, string $responseState): void
    {
        // get and delete the OAuth session information
        if (null === $sessionState = $this->session->take('_oauth2_session')) {
            throw new OAuthException('no state');
        }
        $sessionData = Json::decode($sessionState);

        if (false === hash_equals($sessionData['state'], $responseState)) {
            // the OAuth state from the initial request MUST be the same as the
            // state used by the response
            throw new OAuthException('invalid session (state)');
        }

        // session providerId MUST match current set Provider
        if ($sessionData['provider_id'] !== $provider->providerId()) {
            throw new OAuthException('invalid session (provider_id)');
        }

        // session userId MUST match current set userId
        if ($sessionData['user_id'] !== $userId) {
            throw new OAuthException('invalid session (user_id)');
        }

        // prepare access_token request
        $tokenRequestData = [
            'client_id' => $provider->clientId(),
            'grant_type' => 'authorization_code',
            'code' => $responseCode,
            'redirect_uri' => $sessionData['redirect_uri'],
            'code_verifier' => $sessionData['code_verifier'],
        ];
        if (null !== $clientSecret = $provider->clientSecret()) {
            $tokenRequestData['client_secret'] = $clientSecret;
        }

        $response = $this->httpClient->send(
            Request::post(
                $provider->tokenEndpoint(),
                $tokenRequestData,
                self::getTokenRequestHeaders()
            )
        );

        if (false === $response->isOkay()) {
            throw new TokenException('unable to obtain access_token', $response);
        }

        $this->tokenStorage->storeAccessToken(
            $userId,
            AccessToken::fromCodeResponse(
                $provider,
                $this->dateTime,
                $response->json(),
                // in case server does not return a scope, we know it granted
                // our requested scope (according to OAuth specification)
                new Scope($sessionData['scope'])
            )
        );
    }

    private function refreshAccessToken(Provider $provider, string $userId, AccessToken $accessToken): ?AccessToken
    {
        if (null === $refreshToken = $accessToken->refreshToken()) {
            // we do not have a refresh_token, delete this access token, it
            // is useless now...
            $this->tokenStorage->deleteAccessToken($userId, $accessToken);

            return null;
        }

        // prepare access_token request
        $tokenRequestData = [
            'client_id' => $provider->clientId(),
            'grant_type' => 'refresh_token',
            'refresh_token' => $refreshToken,
            'scope' => (string) $accessToken->scope(),
        ];
        if (null !== $clientSecret = $provider->clientSecret()) {
            $tokenRequestData['client_secret'] = $clientSecret;
        }

        $response = $this->httpClient->send(
            Request::post(
                $provider->tokenEndpoint(),
                $tokenRequestData,
                self::getTokenRequestHeaders()
            )
        );

        if (false === $response->isOkay()) {
            $responseData = $response->json();
            if (\array_key_exists('error', $responseData) && 'invalid_grant' === $responseData['error']) {
                // delete the access_token, we assume the user revoked it, that
                // is why we get "invalid_grant"
                $this->tokenStorage->deleteAccessToken($userId, $accessToken);

                return null;
            }

            throw new TokenException('unable to refresh access_token', $response);
        }

        // delete old AccessToken as we'll write a new one anyway...
        $this->tokenStorage->deleteAccessToken($userId, $accessToken);

        $accessToken = AccessToken::fromRefreshResponse(
            $provider,
            $this->dateTime,
            $response->json(),
            // provide the old AccessToken to borrow some fields if the server
            // does not provide them on "refresh"
            $accessToken
        );

        // store the refreshed AccessToken
        $this->tokenStorage->storeAccessToken($userId, $accessToken);

        return $accessToken;
    }

    /**
     * Find an AccessToken in the list that matches this scope, bound to
     * providerId and userId.
     */
    private function getAccessToken(Provider $provider, string $userId, Scope $scope): ?AccessToken
    {
        $accessTokenList = $this->tokenStorage->accessTokenList($userId);
        foreach ($accessTokenList as $accessToken) {
            if ($provider->providerId() !== $accessToken->providerId()) {
                continue;
            }
            if (!$scope->isSame($accessToken->scope())) {
                continue;
            }

            return $accessToken;
        }

        return null;
    }

    private static function getTokenRequestHeaders(): array
    {
        return [
            'Accept' => 'application/json',
        ];
    }
}
