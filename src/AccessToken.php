<?php

declare(strict_types=1);

/*
 * Copyright (c) 2016-2025 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\OAuth\Client;

use DateInterval;
use DateTimeImmutable;
use fkooman\OAuth\Client\Exception\AccessTokenException;
use RangeException;

class AccessToken
{
    private string $providerId;
    private DateTimeImmutable $issuedAt;
    private string $accessToken;
    private string $tokenType;
    private ?int $expiresIn;
    private ?string $refreshToken;
    private Scope $scope;

    public function __construct(string $providerId, DateTimeImmutable $issuedAt, string $accessToken, string $tokenType, ?int $expiresIn, ?string $refreshToken, Scope $scope)
    {
        $this->providerId = $providerId;
        $this->issuedAt = $issuedAt;
        $this->accessToken = $accessToken;
        $this->tokenType = $tokenType;
        $this->expiresIn = $expiresIn;
        $this->refreshToken = $refreshToken;
        $this->scope = $scope;
    }

    public static function fromCodeResponse(Provider $provider, DateTimeImmutable $dateTime, array $tokenData, Scope $scope): AccessToken
    {
        // if the scope is not part of the response, we'll assume the requested
        // scope was granted.
        try {
            return new self(
                $provider->providerId(),
                $dateTime,
                Extractor::requireString($tokenData, 'access_token'),
                Extractor::requireString($tokenData, 'token_type'),
                Extractor::optionalInt($tokenData, 'expires_in'),
                Extractor::optionalString($tokenData, 'refresh_token'),
                self::determineScope($tokenData, $scope)
            );
        } catch (RangeException $e) {
            throw new AccessTokenException($e->getMessage());
        }
    }

    /**
     * @param AccessToken $accessToken to steal the old scope and refresh_token from!
     */
    public static function fromRefreshResponse(Provider $provider, DateTimeImmutable $dateTime, array $tokenData, self $accessToken): AccessToken
    {
        // (1) if a new refresh_token is not part of the response, we'll keep the
        // existing refresh_token.
        // (2) if the scope is not part of the response, we'll keep using the
        // existing scope.
        try {
            return new self(
                $provider->providerId(),
                $dateTime,
                Extractor::requireString($tokenData, 'access_token'),
                Extractor::requireString($tokenData, 'token_type'),
                Extractor::optionalInt($tokenData, 'expires_in'),
                Extractor::optionalString($tokenData, 'refresh_token') ?? $accessToken->refreshToken(),
                self::determineScope($tokenData, $accessToken->scope())
            );
        } catch (RangeException $e) {
            throw new AccessTokenException($e->getMessage());
        }
    }

    public function providerId(): string
    {
        return $this->providerId;
    }

    public function issuedAt(): DateTimeImmutable
    {
        return $this->issuedAt;
    }

    /**
     * @see https://tools.ietf.org/html/rfc6749#section-5.1
     */
    public function accessToken(): string
    {
        return $this->accessToken;
    }

    /**
     * @see https://tools.ietf.org/html/rfc6749#section-7.1
     */
    public function tokenType(): string
    {
        return $this->tokenType;
    }

    /**
     * @see https://tools.ietf.org/html/rfc6749#section-5.1
     */
    public function expiresIn(): ?int
    {
        return $this->expiresIn;
    }

    /**
     * @return string|null the refresh token
     *
     * @see https://tools.ietf.org/html/rfc6749#section-1.5
     */
    public function refreshToken(): ?string
    {
        return $this->refreshToken;
    }

    /**
     * @see https://tools.ietf.org/html/rfc6749#section-3.3
     */
    public function scope(): Scope
    {
        return $this->scope;
    }

    public function isExpired(DateTimeImmutable $dateTime): bool
    {
        if (null === $expiresIn = $this->expiresIn()) {
            // if no expiry was indicated, assume it is valid
            return false;
        }

        // check to see if issuedAt + expiresIn > provided DateTime
        $expiresAt = $this->issuedAt->add(new DateInterval(\sprintf('PT%dS', $expiresIn)));

        return $dateTime >= $expiresAt;
    }

    public static function fromArray(array $tokenData): self
    {
        try {
            return new self(
                Extractor::requireString($tokenData, 'provider_id'),
                new DateTimeImmutable(Extractor::requireString($tokenData, 'issued_at')),
                Extractor::requireString($tokenData, 'access_token'),
                Extractor::requireString($tokenData, 'token_type'),
                Extractor::optionalInt($tokenData, 'expires_in'),
                Extractor::optionalString($tokenData, 'refresh_token'),
                new Scope(Extractor::requireString($tokenData, 'scope'))
            );
        } catch (RangeException $e) {
            throw new AccessTokenException($e->getMessage());
        }
    }

    public static function fromJson(string $jsonString): self
    {
        return self::fromArray(Json::decode($jsonString));
    }

    public function toJson(): string
    {
        return Json::encode(
            [
                'provider_id' => $this->providerId,
                'issued_at' => $this->issuedAt->format(DateTimeImmutable::ATOM),
                'access_token' => $this->accessToken,
                'token_type' => $this->tokenType,
                'expires_in' => $this->expiresIn,
                'refresh_token' => $this->refreshToken,
                'scope' => (string) $this->scope,
            ]
        );
    }

    private static function determineScope(array $tokenData, Scope $scope): Scope
    {
        if (null === $scopeValue = Extractor::optionalString($tokenData, 'scope')) {
            return $scope;
        }

        return new Scope($scopeValue);
    }
}
