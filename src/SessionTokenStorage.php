<?php

declare(strict_types=1);

/*
 * Copyright (c) 2016-2025 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\OAuth\Client;

class SessionTokenStorage implements TokenStorageInterface
{
    private SessionInterface $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * @return array<AccessToken>
     */
    public function accessTokenList(string $userId): array
    {
        if (null === $v = $this->session->get(\sprintf('_oauth2_token_%s', $userId))) {
            return [];
        }

        $accessTokenList = [];
        $accessTokenDataList = Json::decode($v);
        foreach ($accessTokenDataList as $tokenData) {
            $accessTokenList[] = AccessToken::fromJson($tokenData);
        }

        return $accessTokenList;
    }

    public function storeAccessToken(string $userId, AccessToken $accessToken): void
    {
        $accessTokenList = $this->accessTokenList($userId);
        $accessTokenList[] = $accessToken;

        $this->storeAccessTokenList($userId, $accessTokenList);
    }

    public function deleteAccessToken(string $userId, AccessToken $accessToken): void
    {
        $accessTokenList = $this->accessTokenList($userId);
        foreach ($accessTokenList as $k => $v) {
            if ($accessToken->providerId() === $v->providerId()) {
                if ($accessToken->accessToken() === $v->accessToken()) {
                    unset($accessTokenList[$k]);
                }
            }
        }

        $this->storeAccessTokenList($userId, $accessTokenList);
    }

    /**
     * @param array<AccessToken> $accessTokenList
     */
    private function storeAccessTokenList(string $userId, array $accessTokenList): void
    {
        $tokenDataList = [];
        foreach ($accessTokenList as $accessToken) {
            $tokenDataList[] = $accessToken->toJson();
        }
        $this->session->set(\sprintf('_oauth2_token_%s', $userId), Json::encode($tokenDataList));
    }
}
