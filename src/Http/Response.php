<?php

declare(strict_types=1);

/*
 * Copyright (c) 2016-2025 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\OAuth\Client\Http;

use fkooman\OAuth\Client\Http\Exception\ResponseException;
use fkooman\OAuth\Client\Json;

class Response
{
    private int $statusCode;
    private string $responseBody;

    /** @var array <string,string> */
    private $responseHeaders;

    /**
     * @param array<string,string> $responseHeaders
     */
    public function __construct(int $statusCode, string $responseBody, array $responseHeaders = [])
    {
        $this->statusCode = $statusCode;
        $this->responseBody = $responseBody;
        $this->responseHeaders = $responseHeaders;
    }

    public function __toString(): string
    {
        $responseHeaders = [];
        foreach ($this->responseHeaders as $k => $v) {
            $responseHeaders[] = \sprintf('%s: %s', $k, $v);
        }

        return \sprintf(
            '[statusCode=%d, responseHeaders=[%s], responseBody=%s]',
            $this->statusCode,
            implode(', ', $responseHeaders),
            $this->responseBody
        );
    }

    public function statusCode(): int
    {
        return $this->statusCode;
    }

    public function body(): string
    {
        return $this->responseBody;
    }

    public function hasHeader(string $key): bool
    {
        foreach (array_keys($this->responseHeaders) as $k) {
            if (strtoupper($key) === strtoupper($k)) {
                return true;
            }
        }

        return false;
    }

    public function requireHeader(string $key): string
    {
        foreach ($this->responseHeaders as $k => $v) {
            if (strtoupper($key) === strtoupper($k)) {
                return $v;
            }
        }

        throw new ResponseException(\sprintf('header "%s" not set', $key));
    }

    public function json(): array
    {
        return Json::decode($this->responseBody);
    }

    public function isOkay(): bool
    {
        return 200 <= $this->statusCode && 300 > $this->statusCode;
    }
}
