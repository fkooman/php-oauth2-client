<?php

declare(strict_types=1);

/*
 * Copyright (c) 2016-2025 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\OAuth\Client;

use DateTimeImmutable;
use PDO;

class PdoTokenStorage implements TokenStorageInterface
{
    private PDO $db;

    public function __construct(PDO $db)
    {
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        if ('sqlite' === $db->getAttribute(PDO::ATTR_DRIVER_NAME)) {
            $db->query('PRAGMA foreign_keys = ON');
        }
        $this->db = $db;
    }

    public function init(): void
    {
        $this->db->query(
            <<< EOF
                CREATE TABLE IF NOT EXISTS access_tokens (
                    user_id TEXT NOT NULL,
                    provider_id TEXT NOT NULL,
                    issued_at DATETIME NOT NULL,
                    access_token TEXT NOT NULL,
                    token_type TEXT NOT NULL,
                    expires_in INT,
                    refresh_token TEXT,
                    scope TEXT NOT NULL
                )
                EOF
        );
    }

    /**
     * @return array<AccessToken>
     */
    public function accessTokenList(string $userId): array
    {
        $stmt = $this->db->prepare(
            <<< EOF
                SELECT
                    provider_id,
                    issued_at,
                    access_token,
                    token_type,
                    expires_in,
                    refresh_token,
                    scope
                FROM
                    access_tokens
                WHERE
                    user_id = :user_id
                EOF
        );

        $stmt->bindValue(':user_id', $userId, PDO::PARAM_STR);
        $stmt->execute();

        $accessTokenList = [];
        /** @var array $resultRow */
        foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $resultRow) {
            if (null === $resultRow['expires_in']) {
                unset($resultRow['expires_in']);
            }
            if (null === $resultRow['refresh_token']) {
                unset($resultRow['refresh_token']);
            }
            $accessTokenList[] = AccessToken::fromArray($resultRow);
        }

        return $accessTokenList;
    }

    public function storeAccessToken(string $userId, AccessToken $accessToken): void
    {
        $stmt = $this->db->prepare(
            <<< EOF
                INSERT INTO
                    access_tokens (
                        user_id,
                        provider_id,
                        issued_at,
                        access_token,
                        token_type,
                        expires_in,
                        refresh_token,
                        scope
                    )
                    VALUES (
                        :user_id,
                        :provider_id,
                        :issued_at,
                        :access_token,
                        :token_type,
                        :expires_in,
                        :refresh_token,
                        :scope
                    )
                EOF
        );

        $stmt->bindValue(':user_id', $userId, PDO::PARAM_STR);
        $stmt->bindValue(':provider_id', $accessToken->providerId(), PDO::PARAM_STR);
        $stmt->bindValue(':issued_at', $accessToken->issuedAt()->format(DateTimeImmutable::ATOM), PDO::PARAM_STR);
        $stmt->bindValue(':access_token', $accessToken->accessToken(), PDO::PARAM_STR);
        $stmt->bindValue(':token_type', $accessToken->tokenType(), PDO::PARAM_STR);
        $stmt->bindValue(':expires_in', $accessToken->expiresIn(), PDO::PARAM_INT | PDO::PARAM_NULL);
        $stmt->bindValue(':refresh_token', $accessToken->refreshToken(), PDO::PARAM_STR | PDO::PARAM_NULL);
        $stmt->bindValue(':scope', $accessToken->scope(), PDO::PARAM_STR);
        $stmt->execute();
    }

    public function deleteAccessToken(string $userId, AccessToken $accessToken): void
    {
        $stmt = $this->db->prepare(
            <<< EOF
                DELETE FROM
                    access_tokens
                WHERE
                    user_id = :user_id
                AND
                    provider_id = :provider_id
                AND
                    access_token = :access_token
                EOF
        );

        $stmt->bindValue(':user_id', $userId, PDO::PARAM_STR);
        $stmt->bindValue(':provider_id', $accessToken->providerId(), PDO::PARAM_STR);
        $stmt->bindValue(':access_token', $accessToken->accessToken(), PDO::PARAM_STR);
        $stmt->execute();
    }
}
